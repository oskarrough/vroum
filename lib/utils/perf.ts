export class Perf {
  t0: number;
  title: string;
  steps: [number, string, number?][];

  constructor(title: string = "Benchmark") {
    this.t0 = performance.now();
    this.title = title;
    this.steps = [];
  }

  step(message: string, ops?: number) {
    const time = performance.now();
    this.steps.push([time, message, ops]);
  }

  done() {
    console.log(`=== ${this.title} ===`);

    for (let i = 0; i < this.steps.length; i++) {
      const [lastTime] = i === 0 ? this.steps[0] : this.steps[i - 1];
      const [time, message, ops] = this.steps[i];

      const delta = time - lastTime;
      const deltaBlock = `[${delta.toFixed(3)}ms]`;

      let opsBlock = "";
      if (ops) opsBlock = `(${(ops / delta).toFixed(0)}ops/ms)`;

      console.log(`${deltaBlock} ${message} ${opsBlock}`.trim());
    }

    const totalTime = (performance.now() - this.t0).toFixed(3);
    console.log(`=== Done in ${totalTime} ===`);

    console.log("\n");
    this.steps.length = 0;
  }
}

export default Perf;
