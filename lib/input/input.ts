import Node from "../core/node";
import Html from "../dom/html";

export class Input<P extends {} = {}> extends Node<P> {
  element!: HTMLElement;
  parentInput?: Input;

  action?: string;
  actions: Actions = {};
  buttons: Buttons = {};

  _mount() {
    this.parentInput = this.closest(Input);
    this.element = this.parentInput?.element ?? this.closest(Html)?.element ?? document.body;
    this.element.addEventListener("blur", this.clearInput);
  }

  _destroy(): void {
    this.element.removeEventListener("blur", this.clearInput);
  }

  press(button: string) {
    this.buttons[button] = true;
  }

  release(button: string) {
    delete this.buttons[button];
  }

  dispatchInput(action: string, button?: string) {
    this.dispatch("input", action, button);
    this.parentInput?.dispatchInput(action, button);
  }

  subscribeInput(callback: InputCallback<this>) {
    this.subscribe("input", callback);
  }

  unsubscribeInput(callback: InputCallback<this>) {
    this.unsubscribe("input", callback);
  }

  dispatchAction(action: string, button?: string) {
    this.action = action;
    this.dispatchInput(action, button);
    this.action = undefined;
  }

  isAction(action: string, buttons?: string[]): boolean {
    if (action in this.actions) {
      return this.actions[action](this, buttons);
    }

    if (this.action === action && this.hasButtons(buttons)) {
      return true;
    }

    for (const subinput of this.getAll(Input)) {
      if (subinput.isAction(action, buttons)) return true;
    }

    return false;
  }

  isActive() {
    for (const button in this.buttons) {
      if (this.buttons[button]) return true;
    }
    return false;
  }

  hasButtons(buttons?: string[]) {
    if (!buttons) return true;
    else return buttons.every((button) => this.buttons[button]);
  }

  private clearInput = () => {
    this.action = undefined;
    this.buttons = {};
  };
}

export interface InputCallback<I extends Input> {
  (input: I, action: string, button?: string): void;
}

export interface Actions {
  [key: string]: (Generic: Input, buttons?: string[]) => boolean;
}

export interface Buttons {
  [key: string]: boolean;
}

export default Input;
