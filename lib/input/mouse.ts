import Vec2 from "../2d/vec2";
import Input from "./input";

export class Mouse<P extends {} = {}> extends Input<P> {
  static DOWN = "MOUSE_DOWN";
  static HOLD = "MOUSE_HOLD";
  static UP = "MOUSE_UP";
  static WHEEL = "MOUSE_WHEEL";

  static LEFT = "MouseLeft";
  static MIDDLE = "MouseMiddle";
  static RIGHT = "MouseRight";

  position = Vec2.of(0);
  travel = Vec2.of(0);
  wheel = Vec2.of(0);

  _mount() {
    super._mount();
    this.element.addEventListener("mousedown", this.onMouseDown);
    window.addEventListener("mousemove", this.onMouseMove);
    window.addEventListener("mouseup", this.onMouseUp);
    window.addEventListener("wheel", this.onWheel);
    this.element.addEventListener("contextmenu", this.onContextMenu);
  }

  _destroy() {
    super._destroy();
    this.element.removeEventListener("mousedown", this.onMouseDown);
    window.removeEventListener("mousemove", this.onMouseMove);
    window.removeEventListener("mouseup", this.onMouseUp);
    window.removeEventListener("wheel", this.onWheel);
    this.element.removeEventListener("contextmenu", this.onContextMenu);
  }

  isActive() {
    for (const button in this.buttons) if (this.buttons[button]) return true;
    return false;
  }

  private onMouseDown = (e: MouseEvent) => {
    const button = getMouseButton(e);

    this.press(button);
    this.dispatchAction(Mouse.DOWN, button);

    // set the action as HOLD until all mouse buttons are released
    this.action = Mouse.HOLD;
  };

  private onMouseMove = (e: MouseEvent) => {
    const bbox = this.element.getBoundingClientRect();
    const prevPosition = Vec2.from(this.position);
    this.position.set(e.pageX - bbox.x, e.pageY - bbox.y);
    this.travel.copy(this.position).sub(prevPosition);
  };

  private onMouseUp = (e: MouseEvent) => {
    const button = getMouseButton(e);

    this.dispatchAction(Mouse.UP, button);
    this.release(button);

    if (this.isActive()) {
      this.action = Mouse.HOLD;
    }
  };

  private onWheel = (e: WheelEvent) => {
    this.wheel.set(e.deltaX, e.deltaY);

    this.dispatchAction(Mouse.WHEEL);

    this.wheel.set(0);

    if (this.isActive()) {
      this.action = Mouse.HOLD;
    }
  };

  private onContextMenu = (e: MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };
}

function getMouseButton(e: MouseEvent) {
  switch (e.button) {
    case 0:
      return Mouse.LEFT;
    case 1:
      return Mouse.MIDDLE;
    case 2:
      return Mouse.RIGHT;
    default:
      return "unknown";
  }
}

export default Mouse;
