import Input from "./input";

export class Keyboard<P extends {} = {}> extends Input<P> {
  static DOWN = "KEYBOARD_DOWN";
  static HOLD = "KEYBOARD_HOLD";
  static UP = "KEYBOARD_UP";

  _mount() {
    super._mount();
    this.element.tabIndex = 0;
    this.element.focus();
    this.element.addEventListener("keydown", this.onKeyDown);
    window.addEventListener("keyup", this.onKeyUp);
  }

  _destroy() {
    super._destroy();
    this.element.removeEventListener("keydown", this.onKeyDown);
    window.removeEventListener("keyup", this.onKeyUp);
  }

  private onKeyDown = (e: KeyboardEvent) => {
    // dispatch the DOWN action only on the first time the key was pressed
    if (!e.repeat) {
      this.press(e.key);
      this.dispatchAction(Keyboard.DOWN, e.key);
    }

    // pass the action as HOLD until the keys are released
    this.action = Keyboard.HOLD;
  };

  private onKeyUp = (e: KeyboardEvent) => {
    this.dispatchAction(Keyboard.UP, e.key);
    this.release(e.key);

    if (this.isActive()) {
      this.action = Keyboard.HOLD;
    }
  };
}

export default Keyboard;
