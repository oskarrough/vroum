import { Constructor } from "./node";
import Task from "./task";

/**
 * The Loop class should be used as the root of an application.
 * It manages a loop on which you can schedule tasks.
 * To do so, you can use the Task class and add it in the descendants of this loop.
 *
 * @template P The type of the loop's props.
 */
export class Loop<P extends {} = {}> extends Task<P> {
  /** Whether the loop has been started. */
  started = false;
  /** Whether the loop is paused. */
  paused = false;

  /** The current computer time. */
  time: number | undefined;
  /** The time of execution of the previous frame. */
  lastTime: number | undefined;
  /** The elapsed time since the last frame. */
  deltaTime = 0;
  /** The elapsed time in the playing state. */
  elapsedTime = 0;

  /** A reference to the last frame id. */
  private lastTick: number | undefined;

  /**
   * Create a new instance of the current Loop class and start it right away.
   *
   * @param props Optional props used to instantiate the new loop.
   */
  static start<L extends Loop<any>>(this: Constructor<L>, props?: L["props"]) {
    const loop = new this(props);
    loop.start();
    return loop;
  }

  _mount() {
    super._mount();

    this.started = true;
    this.paused = false;

    this.requestNextTick();
  }

  _destroy(): void {
    this.started = false;
    this.paused = false;

    this.cancelLastTick();
    this.reset();
  }

  /**
   * Starts the loop and connect the node and its descendants
   * @fires Loop#start
   */
  start() {
    if (this.started) return;
    this.connect();
    this.dispatch("start", this);
    return this;
  }

  /**
   * Stops the loop and reset everything
   * @fires Loop#stop
   */
  stop() {
    if (!this.started) return;
    this.disconnect();
    this.dispatch("stop", this);
  }

  /**
   * Resumes the execution of the loop.
   * @fires Loop#play
   */
  play() {
    this.paused = false;
    this.dispatch("play", this);
  }
  /**
   * Puts the loop on hold.
   * @fires Loop#pause
   */
  pause() {
    this.paused = true;
    this.dispatch("pause", this);
  }

  /**
   * Returns the elapsed time since a specified time.
   * @param time The time to compare against.
   * @returns The time passed since the specified time.
   */
  timeSince(time: number) {
    return this.elapsedTime - time;
  }

  /**
   * Requests the next frame of the loop.
   */
  private requestNextTick() {
    this.lastTick = requestTick(this.runTasks);
  }

  /**
   * Cancels the last requested frame of the loop.
   */
  private cancelLastTick() {
    if (this.lastTick) cancelTick(this.lastTick);
  }

  /**
   * Runs all scheduled tasks.
   *
   * @param time The current time.
   * @private
   */
  private runTasks = (time: number) => {
    this.lastTime = this.time ?? time;
    this.time = time;
    this.deltaTime = this.time - this.lastTime;

    if (!this.paused) {
      this.elapsedTime += this.deltaTime; // only increment elapsedTime when the game is in playing state
      const tasks = this.findAll(Task, [this]); // find all the scheduled tasks, including this one

      for (const task of tasks) {
        if (task.isMounted) task.run(this); // run only the mounted tasks
      }
    }

    this.requestNextTick(); // request the next frame of the loop
  };

  /**
   * Resets the timers and counters inside scheduled tasks.
   * @private
   */
  private reset() {
    this.deltaTime = 0;
    this.elapsedTime = 0;
  }
}

function requestTick(callback: (time: number) => void) {
  if (hasAnimationLoop()) return window.requestAnimationFrame(callback);
  else return setTimeout(() => callback(performance.now()), 0);
}

function cancelTick(frame: number) {
  if (hasAnimationLoop()) return window.cancelAnimationFrame(frame);
  else return clearTimeout(frame);
}

function hasAnimationLoop() {
  if (typeof window === undefined) return false;
  else return "requestAnimationFrame" in window;
}

export default Loop;
