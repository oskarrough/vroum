import Emitter from "./emitter";

/**
 * A node in a tree data structure that can be connected to other nodes and emit events.
 *
 * @template P The type of the node's props.
 */
export class Node<P extends {} = {}> extends Emitter {
  /** The props of the node. */
  props: RefProps<this> & P;

  /** The root node of the tree. If this is the root node, it is set to itself. */
  root!: Node;
  /** The parent node of this node, if it has one. */
  parent?: Node;
  /** The child nodes of this node. */
  children: Node[];

  /** Indicates whether the node is currently mounted. */
  isMounted: boolean;

  constructor(props?: Node<P>["props"], children?: Node[]) {
    super();

    this.props = (props ?? {}) as RefProps<this> & P;
    this.children = children ?? [];
    this.isMounted = false;

    if (this.props.ref) {
      this.props.ref.ref = this;
    }
  }

  /**
   * Creates a new `Ref` object that can be used to reference an instance of the node.
   *
   * @template N The type of the node.
   * @returns A `Ref` object.
   */
  static ref<N extends Node>(this: Constructor<N>) {
    return { ref: undefined } as unknown as Ref<N>;
  }

  /**
   * This callback is called when the node is being connected, but its parents and children are not yet available.
   * The `build()` method hasn't been called yet so it's the place where you want to setup properties that you will need during `build()`.
   */
  init?(): void;

  /**
   * When the component is being connected, dynamically build its children using this function.
   * It is useful for creating new classes of Node that always come with specific Nodes inside.
   *
   * @param children The initial list of children passed to the node on instanciation
   */
  build?(children?: Node[]): Node[];

  /**
   * This callback is called once this node is fully connected to its parent and children.
   * It is useful for example to subscribe to events emitted by other Nodes in the tree.
   */
  mount?(): void;

  /**
   * This callback is called right before an update is applied to the props.
   *
   * @param update An object containing the udpated props
   */
  beforeUpdate?(update: Update<this>): void;

  /**
   * This callback is called right after an update is applied to the props.
   *
   * @param update An object containing the udpated props
   */
  afterUpdate?(update: Update<this>): void;

  /**
   * This callback is called right before a node is disconnected.
   * In this callback, the node still has access to its parent and children.
   */
  destroy?(): void;

  // Lifecycle callbacks reserved for vroum logic
  _init?(): void;
  _mount?(): void;
  _beforeUpdate?(update: Update<this>): void;
  _afterUpdate?(update: Update<this>): void;
  _destroy?(): void;

  /**
   * Checks whether the node is of the specified type.
   *
   * @template N The type of the node to check for.
   * @param NodeType The type of the node to check for.
   * @returns `true` if the node is of the specified type, `false` otherwise.
   */
  is<N extends Node>(NodeType: Query<N>): this is N {
    if (!NodeType) return true;
    if (typeof NodeType === "string") return this.constructor.name === NodeType;
    else return this instanceof NodeType;
  }

  /**
   * Returns the first child node that is of the specified type.
   *
   * @template N The type of the node to search for.
   * @param NodeType The type of the node to search for.
   * @returns The first child node that is of the specified type, or `undefined` if no such node is found.
   */
  get<N extends Node>(NodeType: Query<N>): N | undefined {
    for (const child of this.children) {
      if (child.is(NodeType)) return child as N;
    }
    return undefined;
  }

  /**
   * Returns an array of all child nodes that are of the specified type.
   *
   * @template N The type of the nodes to search for.
   * @param NodeType The type of the nodes to search for.
   * @returns An array of all child nodes that are of the specified type.
   */
  getAll<N extends Node>(NodeType: Query<N>): N[] {
    const got: N[] = [];
    for (const child of this.children) {
      if (child.is(NodeType)) got.push(child);
    }
    return got;
  }

  /**
   * Returns the first descendant node that is of the specified type.
   *
   * @template N The type of the node to search for.
   * @param NodeType The type of the node to search for.
   * @returns The first descendant node that is of the specified type, or `undefined` if no such node is found.
   */
  find<N extends Node>(NodeType: Query<N>): N | undefined {
    for (const child of this.children) {
      if (child.is(NodeType)) return child;
      const found = child.find(NodeType);
      if (found) return found;
    }
    return undefined;
  }

  /**
   * Returns an array of all descendant nodes that are of the specified type.
   *
   * @template N The type of the nodes to search for.
   * @param NodeType The type of the nodes to search for.
   * @param found An optional array to append the found nodes to.
   * @returns An array of all descendant nodes that are of the specified type.
   */
  findAll<N extends Node>(NodeType?: Query<N>, found: N[] = []) {
    for (const child of this.children) {
      if (child.is(NodeType)) found.push(child);
      child.findAll(NodeType, found);
    }
    return found;
  }

  /**
   * Finds the closest parent node that matches the provided type or constructor function.
   *
   * @template N The type of node to find.
   * @param NodeType The type of node to find.
   * @returns The matching node or undefined if none found.
   */
  closest<N extends Node>(NodeType: Query<N>): N | undefined {
    if (this.parent?.is(NodeType)) return this.parent;
    else if (this.parent) return (this.parent as Node).closest(NodeType);
    else return undefined;
  }

  /**
   * Adds one or more child nodes to this node.
   *
   * @param nodes The child nodes to add.
   */
  add(...nodes: Node[]) {
    for (const child of nodes) {
      child.connect(this);
    }
  }

  /**
   * Removes one or more child nodes from this node.
   *
   * @param nodes The child nodes to remove.
   */
  remove(...nodes: Node[]) {
    for (const node of nodes) {
      node.disconnect(this);
    }
  }

  /**
   * Updates the node's properties with the provided values.
   *
   * @param update An object containing the properties to update.
   */
  set(update: Update<this>) {
    this._beforeUpdate?.(update);
    this.beforeUpdate?.(update);
    Object.assign(this.props, update);
    this._afterUpdate?.(update);
    this.afterUpdate?.(update);
  }

  /**
   * Connects this node to a parent node and builds its children.
   * The mount callback should be triggered when connecting a root (= no parent)
   * or when connecting to a node that is already mounted.
   *
   * @param parent The parent node to connect to.
   * @param shouldMount Whether to trigger the mount callback.
   */
  connect(parent?: Node, shouldMount = !parent || parent.isMounted) {
    // remove from previous parent if different
    if (this.parent && this.parent !== parent) {
      removeFromParent(this);
    }

    this._init?.();
    this.init?.();

    // set new parent and root
    this.parent = parent;
    this.root = parent?.root ?? this;

    // add this node as child of the parent if not already there
    if (this.parent && this.parent.children.indexOf(this) < 0) {
      this.parent.children.push(this);
    }

    // build the children nodes if specified
    if (this.build) {
      this.children = this.build(this.children);
    }

    // connect all children recursively
    for (const child of this.children) {
      child.connect(this, shouldMount);
    }

    // call mount once everything is connected
    if (shouldMount) {
      this.isMounted = true;
      this._mount?.();
      this.mount?.();
    }
  }
  /**
   * Disconnects this node from its parent node and destroys it along with its descendants.
   *
   * @param {Node} [parent] The parent node to disconnect from.
   */
  disconnect(parent?: Node) {
    // if a parent is specified, make sure this node really is a child of it
    if (parent && this.parent !== parent) return;

    if (this.isMounted) {
      this.destroy?.();
      this._destroy?.();
    }

    // remove from parent
    if (this.parent) {
      removeFromParent(this);
    }

    // disconnect children recursively
    for (const child of Array.from(this.children)) {
      child.disconnect();
    }

    // mark the node as not mounted once everything is cleaned up
    this.isMounted = false;
  }
}

function removeFromParent(node: Node) {
  if (!node.parent) return;

  const siblings = node.parent.children;
  node.parent = undefined;

  const index = siblings.indexOf(node);
  if (index >= 0) siblings.splice(index, 1);
}

export interface RefProps<N extends Node> {
  ref?: Ref<N>;
}

export interface Ref<N extends Node> {
  ref: N;
}

export type Update<N extends Node> = Partial<N["props"]>;

export interface Constructor<T> {
  new (...args: any): T;
}

export type Query<N extends Node> = Constructor<N> | string | undefined;

export default Node;
