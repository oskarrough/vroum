import Loop from "./loop";
import Node from "./node";

export interface TaskProps {
  /** A function that will be called every tick of the Task. */
  tick?: (loop: Loop, task: Task) => void;
  /** The amount of time to wait before starting the first cycle of the task (0 = immediately). */
  delay?: number;
  /** The amount of time to wait between two successive cycles (0 = immediately). */
  interval?: number;
  /** The amount of time during which the task will run for every tick (0 = 1 call per cycle). */
  duration?: number;
  /** The number of cycles of the task that should happen (Infinity = forever).  */
  repeat?: number;
  /** The number of ticks of the task that should run per cycle. */
  limit?: number;
}

/**
 * A Task instance that is used to manage and run tasks in a loop.
 *
 * @template P The type of the task's props.
 */
export class Task<P extends {} = {}> extends Node<TaskProps & P> {
  loop!: Loop;

  /** The amount of time to wait before starting the first cycle of the task */
  delay!: number;
  /** The amount of time to wait between two successive cycles (0 = immediately). */
  interval!: number;
  /** The amount of time during which the task will run for every tick (0 = 1 call per cycle). */
  duration!: number;
  /** The number of cycles of the task that should happen (Infinity = forever). */
  repeat!: number;
  /** The max number of ticks of the task that should run per cycle. */
  limit!: number;

  /** The time at which the task was registered. */
  startTime!: number;
  /** The total number of full cycles executed. */
  cycles!: number;
  /** The total number of single calls of the tick callback for the current cycle. */
  ticks!: number;

  _init() {
    // prepare the tick callback
    this.tick = this.tick ?? this.props.tick;

    // by default, repeat for ever on every requested tick, without waiting inbetween
    this.delay = this.props.delay ?? this.delay ?? 0;
    this.interval = this.props.interval ?? this.interval ?? 0;
    this.duration = this.props.duration ?? this.duration ?? 0;
    this.repeat = this.props.repeat ?? this.repeat ?? Infinity;
    this.limit = this.props.limit ?? this.limit ?? (this.duration > 0 ? Infinity : 1);
  }

  _mount() {
    // reference the closest loop for quick access
    this.loop = this.is(Loop) ? this : this.closest(Loop)!;

    // initialize task timing
    this.startTime = this.loop.elapsedTime;
    this.cycles = 0;
    this.ticks = 0;
  }

  /**
   * Run the Task instance.
   * Check the time passed, and run the task if the timing is right.
   * Increment the cycle and tick counters as necessary.
   * Disconnect the Task instance from the tree if the task cycles have been repeated enough.
   *
   * @param loop The Loop instance that is managing the Task.
   */
  run(loop: Loop) {
    // use the total time passed on this task so we can make it tick faithfully
    const taskTime = loop.elapsedTime - this.startTime;

    const startTime = this.delay + (this.duration + this.interval) * this.cycles;
    const runTime = startTime + this.duration;

    // we leave an imprecision of 1 to deal with frame duration variation
    const isDelayPassed = taskTime - startTime >= -1;
    const isDurationPassed = taskTime - runTime >= -1;

    const isLimitPassed = this.ticks >= this.limit;
    const isInstant = this.duration === 0;

    // actually run the task if the timing is right
    if (isDelayPassed && !isLimitPassed && (isInstant || !isDurationPassed)) {
      if (this.ticks === 0) {
        if (this.cycles === 0) {
          this._begin?.(loop, this);
          this.begin?.(loop, this);
        }

        this._beforeCycle?.(loop, this);
        this.beforeCycle?.(loop, this);
      }

      this._tick?.(loop, this);
      this.tick?.(loop, this);
      this.ticks += 1;
    }

    // increment the cycle counter when the task has reached the end of its current cycle
    if (isDurationPassed) {
      this._afterCycle?.(loop, this);
      this.afterCycle?.(loop, this);

      this.cycles += 1;
      this.ticks = 0;
    }

    // task cycles have been repeated enough, we disconnect the Task from the tree
    if (this.cycles >= this.repeat) {
      this._end?.(loop, this);
      this.end?.(loop, this);
      this.disconnect();
    }
  }

  /**
   * Lifecycle callback executed when the initial cycle of the task begins.
   *
   * @param loop The `Loop` object.
   * @param task The current `Task` object.
   */
  begin?(loop: Loop, task: Task): void;

  /**
   * Lifecycle callback executed just before any new cycle begins
   *
   * @param loop The `Loop` object.
   * @param task The current `Task` object.
   */
  beforeCycle?(loop: Loop, task: Task): void;

  /**
   * The `tick` method is called on every tick of the `Task`.
   *
   * @param loop The `Loop` object.
   * @param task The current `Task` object.
   */
  tick?(loop: Loop, task: Task): void;

  /**
   * Lifecycle callback executed just after any new cycle ends
   *
   * @param loop The `Loop` object.
   * @param task The current `Task` object.
   */
  afterCycle?(loop: Loop, task: Task): void;

  /**
   * Lifecycle callback executed just after the final cycle of the task ended
   *
   * @param loop The `Loop` object.
   * @param task The current `Task` object.
   */
  end?(loop: Loop, task: Task): void;

  // tick callback reserved for internal logic
  _begin?(loop: Loop, task: Task): void;
  _beforeCycle?(loop: Loop, task: Task): void;
  _tick?(loop: Loop, task: Task): void;
  _afterCycle?(loop: Loop, task: Task): void;
  _end?(loop: Loop, task: Task): void;
}

export default Task;
