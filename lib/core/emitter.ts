/**
 * A class for managing and emitting events.
 */
export class Emitter {
  /** An object to store registered event callbacks by event names. */
  protected events: Record<string, EventCallback<any>[]> = {};

  /**
   * Dispatches an event to all registered callbacks.
   *
   * @param event The name of the event to dispatch.
   * @param data The data to pass to the registered callbacks.
   */
  dispatch(event: string, ...data: any[]): void {
    if (!this.events[event]) return;
    for (const callback of this.events[event]) {
      callback.call(this, this, ...data);
    }
  }

  /**
   * Registers a callback for the given event.
   *
   * @param event The name of the event to subscribe to.
   * @param callback The callback function to register.
   */
  subscribe(event: string, callback: EventCallback<this>): void {
    if (!this.events[event]) this.events[event] = [];
    if (this.events[event].indexOf(callback) >= 0) return;
    this.events[event].push(callback);
  }

  /**
   * Unregisters a callback for the given event.
   *
   * @param event The name of the event to unsubscribe from.
   * @param callback The callback function to unregister.
   */
  unsubscribe(event: string, callback?: EventCallback<this>): void {
    if (!this.events[event]) return;

    if (!callback) {
      this.events[event].length = 0;
      return;
    }

    const index = this.events[event].indexOf(callback);
    this.events[event].splice(index, 1);
  }
}

/**
 * A callback function for an event.
 *
 * @template N The type of the Emitter.
 * @template T The type of the data to be passed to the callback.
 * @param node The Emitter that triggered the event.
 * @param data The data passed to the callback.
 */
export interface EventCallback<N extends Emitter, T extends any[] = any[]> {
  (node: N, ...data: T): void;
}

export default Emitter;
