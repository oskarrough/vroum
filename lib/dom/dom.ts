import Node, { Update } from "../core/node";

export interface DomProps {
  tag?: string;
  container?: Element;
  [key: string]: any;
}

export class Dom<E extends Element, P extends {} = {}> extends Node<DomProps & P> {
  tag?: string;
  container?: Element;
  element!: E;

  _init() {
    this.tag = this.props.tag ?? this.tag;
    this.container = this.props.container ?? this.container;
    this.element = this.createElement!();
  }

  _mount() {
    this.updateElement(this.props);
    if (!this.container) this.container = this.closest(Dom)?.element ?? document.body;
    this.container?.append(this.element);
  }

  _beforeUpdate(update: Update<this>) {
    this.removeEventListeners(update);
  }

  _afterUpdate(update: Update<this>) {
    this.updateElement(update);
  }

  _destroy() {
    this.removeEventListeners(this.props);
    this.element.remove();
  }

  protected createElement?(): E;

  private updateElement(props: Update<this>) {
    for (const prop in props) {
      // prop is for an event listener
      if (isEventListener(prop)) {
        this.element.addEventListener(prop.slice(1), props[prop]!);
      }
      // prop is an element property
      else if (isProperty(prop)) {
        const property = prop.slice(1) as keyof typeof this.element;
        this.element[property] = props[prop]!;
      }
      // prop is an element attribute
      else if (isAttribute(prop)) {
        this.setAttribute(prop, props[prop]);
      }
    }
  }

  private removeEventListeners(props: Update<this>) {
    for (const prop in props) {
      if (prop[0] === "$") {
        this.element.removeEventListener(prop.slice(1), props[prop]!);
      }
    }
  }

  private setAttribute(attribute: string, value: any) {
    let domValue: string | undefined;

    if (typeof value === "boolean") domValue = value ? "" : undefined;
    else if (value !== null && value !== undefined) domValue = String(value);

    if (domValue !== undefined) this.element.setAttribute(attribute, domValue);
    else this.element.removeAttribute(attribute);
  }
}

function isProperty(prop: string) {
  return prop[0] === "_";
}

function isEventListener(prop: string) {
  return prop[0] === "$";
}

function isAttribute(prop: string) {
  return prop !== "ref";
}

export default Dom;
