import Node, { Ref } from "../core/node";
import Dom from "./dom";

export interface HtmlProps<T extends HtmlTag> {
  tag?: T;
}

export class Html<T extends HtmlTag, P extends {} = {}> extends Dom<
  HtmlElement<T>,
  HtmlProps<T> & P
> {
  declare tag?: T;
  declare element: HtmlElement<T>;

  protected createElement() {
    return document.createElement(this.tag ?? "div") as HtmlElement<T>;
  }
}

export function html<T extends HtmlTag>(tag: T, props?: Html<T>["props"], children?: Node[]) {
  return new Html({ tag, ...props }, children);
}

export type HtmlTag = keyof HTMLElementTagNameMap;
export type HtmlElement<T extends HtmlTag> = HTMLElementTagNameMap[T];
export type HtmlRef<T extends HtmlTag> = Ref<Html<T>>;

export default Html;
