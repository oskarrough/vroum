import Node, { Ref } from "../core/node";
import Dom from "./dom";

export interface SvgProps<T extends SvgTag> {
  tag?: T;
}

export class Svg<T extends SvgTag, P extends {} = {}> extends Dom<SvgElement<T>, SvgProps<T> & P> {
  declare tag?: T;
  declare element: SvgElement<T>;

  protected createElement() {
    return document.createElementNS("http://www.w3.org/2000/svg", this.tag ?? 'svg') as SvgElement<T>; // prettier-ignore
  }
}

export function svg<T extends SvgTag>(tag: T, props?: Svg<T>["props"], children?: Node[]) {
  return new Svg({ tag, ...props }, children);
}

export type SvgTag = keyof SVGElementTagNameMap;
export type SvgElement<T extends SvgTag> = SVGElementTagNameMap[T];
export type SvgRef<T extends SvgTag> = Ref<Svg<T>>;

export default Svg;
