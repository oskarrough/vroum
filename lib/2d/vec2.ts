export interface Coords {
  x: number;
  y: number;
}

export function point(x: number, y: number): Coords {
  return { x, y };
}

export class Vec2 implements Coords {
  static ZERO: Coords = Object.freeze({ x: 0, y: 0 });

  x: number;
  y: number;

  constructor(x: number, y: number = x) {
    this.x = x;
    this.y = y;
  }

  static of(x: number, y?: number) {
    return new Vec2(x, y);
  }

  static from(v: Coords) {
    return new Vec2(v.x, v.y);
  }

  static center(origin: Coords, dimensions: Coords) {
    return Vec2.from(origin).add(Vec2.from(dimensions).scale(1 / 2));
  }

  static origin(center: Coords, dimensions: Coords) {
    return Vec2.from(center).sub(Vec2.from(dimensions).scale(1 / 2));
  }

  static normal(u: Coords, v: Coords) {
    const dx = v.x - u.x;
    const dy = v.y - u.y;
    return Vec2.of(-dy, dx).unit();
  }

  norm() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  dot(v: Coords) {
    return this.x * v.x + this.y * v.y;
  }

  cross(v: Coords) {
    return this.x * v.y - v.x * this.y;
  }

  equals(v: Coords) {
    return this.x === v.x && this.y === v.y;
  }

  is(x: number, y: number = x) {
    return this.x === x && this.y === y;
  }

  apply(x: (x: number) => number, y: (y: number) => number = x) {
    this.x = x(this.x);
    this.y = y(this.y);
    return this;
  }

  coords(): Coords {
    return { x: this.y, y: this.y };
  }

  unit() {
    return this.scale(1 / this.norm());
  }

  copy(v: Coords) {
    this.x = v.x;
    this.y = v.y;
    return this;
  }

  set(x: number, y: number = x) {
    this.x = x;
    this.y = y;
    return this;
  }

  min(v: Coords) {
    this.x = Math.min(this.x, v.x);
    this.y = Math.min(this.y, v.y);
    return this;
  }

  max(v: Coords) {
    this.x = Math.max(this.x, v.x);
    this.y = Math.max(this.y, v.y);
    return this;
  }

  clamp(min: Coords, max: Coords) {
    return this.max(min).min(max);
  }

  add(v: Coords) {
    this.x += v.x;
    this.y += v.y;
    return this;
  }

  sub(v: Coords) {
    this.x -= v.x;
    this.y -= v.y;
    return this;
  }

  mul(v: Coords) {
    this.x *= v.x;
    this.y *= v.y;
    return this;
  }

  div(v: Coords) {
    this.x /= v.x;
    this.y /= v.y;
    return this;
  }

  translate(x: number, y: number = x) {
    this.x += x;
    this.y += y;
    return this;
  }

  scale(x: number, y: number = x) {
    this.x *= x;
    this.y *= y;
    return this;
  }

  rotate(angle: number, center: Coords = Vec2.ZERO) {
    const x = this.x - center.x;
    const y = this.y - center.y;

    const cos = Math.cos(angle);
    const sin = Math.sin(angle);

    this.x = cos * x - sin * y + center.x;
    this.y = sin * x + cos * y + center.y;

    return this;
  }
}

export default Vec2;
