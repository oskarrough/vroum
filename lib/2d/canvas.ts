import Html from "../dom/html";

interface CanvasProps {
  width: number;
  height: number;
}

export class Canvas<P extends {} = {}> extends Html<"canvas", CanvasProps & P> {
  tag = "canvas" as const;

  ctx!: CanvasRenderingContext2D;
  width!: number;
  height!: number;

  _mount() {
    super._mount();
    this.ctx = this.element.getContext("2d")!;
    this.width = this.props.width ?? window.innerWidth;
    this.height = this.props.height ?? window.innerHeight;
  }

  clearCanvas() {
    this.ctx.clearRect(0, 0, this.width, this.height);
  }
}

export default Canvas;
