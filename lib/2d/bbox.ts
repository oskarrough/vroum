import Task from "../core/task";
import Vec2 from "./vec2";

export interface BBoxProps {
  position?: Vec2;
  dimensions?: Vec2;
  static?: boolean;
}

export class BBox<P extends {} = {}> extends Task<BBoxProps & P> {
  position!: Vec2;
  dimensions!: Vec2;
  static!: boolean;
  colliding!: boolean;

  private adjustments!: Vec2[];

  _init() {
    super._init();
    this.position = this.props.position ?? this.position ?? Vec2.of(0);
    this.dimensions = this.props.dimensions ?? this.dimensions ?? Vec2.of(0);
    this.static = this.props.static ?? this.static ?? false;
    this.colliding = false;
    this.adjustments = [];
  }

  _tick() {
    this.colliding = false;
    if (this.static) return;

    const nodes = this.root.findAll(BBox);
    const adjustments = this.checkCollisions(nodes);

    for (const adjustment of adjustments) {
      this.position.add(adjustment);
    }
  }

  checkCollisions(nodes: BBox[]) {
    this.adjustments.length = 0;

    for (const other of nodes) {
      if (this === other) continue;
      if (!this.hasOverlap(other)) continue;

      this.colliding = true;
      other.colliding = true;

      this.dispatchCollision(other);

      const adjustement = this.getOverlapAdjustment(other);
      this.adjustments.push(adjustement);
    }

    return this.adjustments;
  }

  dispatchCollision(other: BBox) {
    this.dispatch("collision", other);
  }

  subscribeCollision(callback: (self: BBox, other: BBox) => void) {
    this.subscribe("collision", callback);
  }

  unsubscribeCollision(callback: (self: BBox, other: BBox) => void) {
    this.unsubscribe("collision", callback);
  }

  hasOverlap(node: BBox) {
    const halfA = Vec2.from(this.dimensions).scale(1 / 2);
    const a1 = Vec2.from(this.position).sub(halfA); // this top left
    const a2 = Vec2.from(this.position).add(halfA); // this bottom right

    const halfB = Vec2.from(node.dimensions).scale(1 / 2);
    const b1 = Vec2.from(node.position).sub(halfB); // node top left
    const b2 = Vec2.from(node.position).add(halfB); // node bottom right

    return (
      a1.x < b2.x &&
      a2.x > b1.x &&
      a1.y < b2.y && //
      a2.y > b1.y
    );
  }

  getOverlapAdjustment(node: BBox) {
    const impulse = Vec2.of(0);

    // Calculate the vector from centerA to centerB
    const diff = Vec2.from(this.position).sub(node.position);
    const absDiff = Vec2.from(diff).apply(Math.abs);

    // get the half dimensions
    const halfDimensions = Vec2.from(this.dimensions).add(node.dimensions).scale(1 / 2); // prettier-ignore

    // Calculate the magnitude of the impulse vector
    const overlap = Vec2.from(halfDimensions).sub(absDiff);

    // push back the smallest distance required to avoid the overlap
    if (overlap.x < overlap.y) {
      impulse.x = Math.sign(diff.x) * overlap.x;
    } else {
      impulse.y = Math.sign(diff.y) * overlap.y;
    }

    return impulse;
  }
}

export default BBox;
