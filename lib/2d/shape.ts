import Task from "../core/task";
import Vec2 from "./vec2";
import Canvas from "./canvas";

export interface ShapeProps {
  position?: Vec2;
}

export class Shape<P extends {} = {}> extends Task<ShapeProps & P> {
  position!: Vec2;

  private canvas!: Canvas;

  _mount() {
    super._mount();
    this.position = this.props.position ?? this.position ?? Vec2.of(0);
    this.canvas = this.closest(Canvas)!;
    if (!this.canvas) throw new Error("Parent canvas not found");
  }

  _tick() {
    this.paint?.(this.canvas.ctx);
  }

  paint?(ctx: CanvasRenderingContext2D): void;
}

export default Shape;
