# VROUM

The initial goal of this library was to provide a way to define reusable components that could be painted on a `canvas` at 60fps.

It then evolved into something less specific and now tries to provide the following features:
- Abstract specific behaviors by encapsulating them in reusable node classes.
- Connect these nodes as a tree to compose them into more complex behaviors.
- Schedule actions precisely by defining tasks that run on a timed loop.

## Example

The following program will run a loop and log the elapsed time at regular intervals.

```ts
import { Loop, Task } from "vroum/core";

class App extends Loop {
  build() {
    return [new LogTask()]
  }
}

class LogTask extends Task {
  delay = 500; // wait 500ms before writing the first log
  duration = 1000; // then log on every frame for 1s
  interval = 2000; // once the duration has passed, wait for 2s before starting the next cycle of logs
  repeat = 3; // repeat the log cycles 3 times and then stop

  tick(loop: Loop) {
    console.log(`${loop.elapsedTime}ms have passed.`)
  }
}

App.start();
```

## Installation

Run `npm install vroum` to install the library in your project.

You can them import its contents in two ways:
- Use the global bundle that has all `vroum` features in it: `import { Node } from "vroum"`
- Use sub-bundles to only get specific parts of the lib: `import { Canvas } from "vroum/2d"`

The available sub-bundles are: `vroum/core`, `vroum/dom`, `vroum/2d` and `vroum/input`.

# Table of contents

- [VROUM](#vroum)
  - [Example](#example)
  - [Installation](#installation)
- [Table of contents](#table-of-contents)
- [Core](#core)
  - [Node](#node)
    - [Props](#props)
    - [Refs](#refs)
    - [Class properties](#class-properties)
    - [Lifecycle](#lifecycle)
    - [Prop management](#prop-management)
    - [Tree manipulation](#tree-manipulation)
    - [Search and match](#search-and-match)
    - [Event management](#event-management)
  - [Loop](#loop)
    - [Properties](#properties)
    - [Static methods](#static-methods)
    - [Methods](#methods)
  - [Task](#task)
    - [Properties](#properties-1)
    - [Lifecycle](#lifecycle-1)
- [DOM](#dom)
- [2D](#2d)
  - [Canvas](#canvas)
    - [Properties](#properties-2)
    - [Methods](#methods-1)
  - [Shape](#shape)
    - [Properties](#properties-3)
    - [Methods](#methods-2)
- [Input](#input)
  - [Input](#input-1)
    - [Methods](#methods-3)
  - [Mouse](#mouse)
    - [Actions](#actions)
    - [Buttons](#buttons)
    - [Properties](#properties-4)
  - [Keyboard](#keyboard)
    - [Actions](#actions-1)

# Core

This library provides you with 3 classes that you can extend and compose in order to organize your application:
- `Node`: the base for everything, it deals with managing lifecycle and organizing the tree
- `Loop`: a special `Node` that manages a loop that ticks at regular time intervals to synchronize `Task` nodes
- `Task`: as special `Node` that can define a schedule to precisely execute some actions whenever wanted.

Note that all these classes can actually be used server-side (in node or deno) as they do not require any web APIs.

## Node

The core of the library is the `Node` class. It is the fundamental building block of a `vroum` app.
Any other part of the lib is actually an extension of the `Node` class (even `Loop` and `Task`), so that everything in your program can be organized as a `Node` tree.

You can give children to a `Node` in a few ways:
- During instantiation:
```ts
const node = new Node({}, [
  new Node(),
  new Node(),
  new Node(),
])
```

- At the class level with the `build` method:
```ts
class MyNode extends Node {
  build(children: Node[]) {
    return [
      new Node(),
      new Node(),
      new Node()
    ]
  }
}
```

- By adding children to a node:
```ts
const parent = new Node()
const child1 = new Node()
const child2 = new Node()

node.add(child1, child2)
```

- By connecting a node to a parent
```ts
const parent = new Node()
const child = new Node()

child.connect(parent)
```

### Props

- When instanciating a `Node`, you can pass it props as first argument and they will be stored inside `this.props`. The `Node` class actually has a generic param `Node<P extends {}>` that can be given to specify the exact type of a node's props.
```ts
props: P;
```

### Refs

You can create refs to nodes by using the static method `ref()` of the `Node` class. The `ref()` static method is also available on all classes that extend `Node`, in which case they return a ref type that matches the current class.

You can then pass these refs as the `ref` prop of your nodes. When the node is instanciated, it puts a reference to itself inside the ref prop, so it becomes accessible where the ref was defined initially.

A ref object has the following interface:
```ts
interface Ref<N extends Node> { ref: N; }
```

And here is a simple example to illustrate:

```ts
class App extends Node {
  customNode = CustomNode.ref()

  mount() {
    console.log(this.customNode.ref.customProperty) // logs "12"
  }

  build() {
    return [new CustomNode({ ref: this.customNode })]
  }
}

class CustomNode extends Node {
  customProperty = 12
}
```

### Class properties

- The `root` property is a direct reference to the root of the whole `Node` tree.
```ts
root: Node;
```

- The `parent` property is a direct reference to the `Node` that is just one level above this one in the tree.
```ts
parent?: Node;
```

- The `children` property is a list of direct references to the `Nodes` that are connected to this one on a lower level.
```ts
children: Node[] = [];
```

### Lifecycle

To guarantee that your code is ran at points in time where some specific data is available, the `Node` class offers a collection of lifecycle callbacks. To use them, you need to create a new class that extends `Node`, and define your own callbacks by implementing methods named after the different lifecycle steps.

To boot the lifecycle, you first need to call the `connect()` method of a `Node`. Then, the lifecycle callbacks of all the nodes descending from the root will run synchronously, so your whole tree is completely ready by the time the call to `connect()` ends.

The callbacks are executed in the following order:

- Called directly when a node starts connecting. Here you should prepare class properties so they can be used in other lifecycle callbacks or in `build()`.
```ts
init(): void;
```

- If this method is implemented, when the node calls `connect()`, it will build itself a new list of children using this function. As argument, it receives the original list of children given to the node at instantiation time, so they can be placed correctly inside the newly built children.
```ts
build(children?: Node[]): Node[];
```

- Called after the whole tree of nodes related to this one is mounted, and all the parent/children links between nodes are setup. Useful for finding references to other nodes in the tree and setting up event callbacks.
```ts
mount(loop: Loop): void;
```

- Called when a component is disconnected, but before it loses its connection to the tree, so you can clean up what was done in previous lifecycle callbacks.
```ts
destroy(loop: Loop): void;
```

### Prop management

There is a special lifecycle management related to changes in props. To actually register updates in a node's props, you have to use its `set()` method.

The `Update` type you see below is just some sugar and is equivalent to the type of the current node props, but with all members optional so you can change a subset of props in one operation.

By overriding the `beforeUpdate`/`afterUpdate` methods of your extended `Node`, you will be able to run code synchronously before/after the update is applied to the node.

- Updates the node's props with the provided values.
```ts
set(update: Update<this>): void
```

- This callback is called right before an update is applied to the props.
```ts
beforeUpdate(update: Update<this>): void;
```

- This callback is called right after an update is applied to the props.
```ts
afterUpdate(update: Update<this>): void;
```

### Tree manipulation

The following methods allow manipulating the content of the tree by connecting nodes as parents and children.

- Add a list of children to this node and connect them to it
```ts
add(...nodes: Node[]): void;
```

- Remove a list of children from this node and disconnect them from it
```ts
remove(...nodes: Node[]): void;
```

- Recursively build the node and its children, and run the lifecycle callbacks in the right order. The `mount()` callback will only be called if no parent is specified (this means we're connecting a root node), or if the given parent is already mounted.
```ts
connect(parent?: Node): void
```

- Prepare the node and all its descendants to be destroyed. Specifying a parent checks that the node really is a child of this parent before removing it. Without it, the node will simply remove itself from its current parent.
```ts
disconnect(parent?: Node): void
```

### Search and match

The following methods help you find nodes in the tree.
When you see the `Query` type, it means you can either use directly a class extending `Node`, or a string matching the name of a `Node` class.

```ts
const loop = new Loop()
const task = new Task()

loop.add(task)

const foundTask = loop.get(Task) // returns a node of type Task
assert(foundTask === task)

// this works as well
const foundTask = loop.get("Task") // returns a node of type Node, TS can't get the true type from just a string
assert(foundTask === task)
```

- Checks if the current `Node` matches the given query.
```ts
is<N extends Node>(NodeType: Query<N>): boolean;
```

- Get the first direct child matching the query.
```ts
get<N extends Node>(NodeType: Query<N>): N | undefined;
```

- Get all the direct children matching the query.
```ts
getAll<N extends Node>(NodeType: Query<N>): N[];
```

- Recursively look for a node matching the query in the descendants of this node.
```ts
find<N extends Node>(NodeType: Query<N>): N | undefined;
```

- Recursively look for all the nodes matching the query in the descendants of this node.
```ts
findAll<N extends Node>(NodeType?: Query<N>, found?: N[]): N[];
```

- Find the closest ancestor that matches the query.
```ts
closest<N extends Node>(NodeType: Query<N>): N | undefined
```

### Event management

The `Node` class is also an event emitter so you can both subscribe to events and dispatch them using the emitter API.

When dispatching an event, the first argument of the event callbacks that will be called will be the node that triggered the event.
If you pass extra arguments after the event to the `dispatch()` function, they all will be spread as the rest of the arguments of this event's callbacks.

Note that event callbacks are always called synchronously when dispatching an event from a node.

- Dispatches an event to all registered callbacks.
```ts
dispatch(event: string, ...data: any[]): void
```

- Registers a callback for the given event.
```ts
subscribe(event: string, callback: EventCallback<this>): void
```

- Unregisters a callback for the given event.
```ts
unsubscribe(event: string, callback?: EventCallback<this>): void
```

## Loop

`Loop` is a special kind of `Node` that, as its name suggests, runs a loop. It uses `requestAnimationFrame` (and fallbacks to `setTimeout` if not available) to manage this loop and keep track of time, so you can schedule actions using the related `Task` class.

### Properties

- If the loop was started
```ts
started: boolean;
```

- If the loop is paused
```ts
paused: boolean;
```

- Current computer time
```ts
time: number | undefined;
```

- Computer time of the last frame
```ts
lastTime: number | undefined;
```

- Elapsed time since last frame
```ts
deltaTime: number;
```

- Elapsed time in playing state (not paused)
```ts
elapsedTime: number;
```

### Static methods

- Create a new instance of the current Loop class and start ticking right away.
```ts
static start<L extends Loop>(this: Constructor<L>, props?: L["props"]): L
```

### Methods

- Connect the loop and its children and start looping
```ts
start(): void
```

- Disconnect the loop and its children and stop looping
```ts
stop(): void
```

- Resume playing the loop
```ts
play(): void
```

- Puts the loop on hold
```ts
pause(): void
```

- Get the time elapsed since the given time
```ts
timeSince(time: number): number
```

## Task

The `Task` class is another type of `Node` that allows you to schedule actions in your app.
As long as a `Task` is inside your node tree, it will try to run its `tick()` method. Wether or not the `tick()` callback will be called depends on the properties you give to the task.

In the following descriptions, you will often see the term "cycle" used. It represents the series of consecutive frames during which your task will call its `tick()` callback. In the example below, for the `[duration]` time span, the task will call its `tick()` callback on every frame. But when inside `[delay]` or `[interval]`, the task will ignore the frames and so won't tick. In other words, a cycle is the group of all the `tick()` callback calls that happen during the specified `duration`.

The `Task` will schedule its tick in the following way:
```
[delay] [duration] [interval] [duration] [interval] [duration] ...
 ^ wait  ^ ticks    ^ wait     ^ ticks    ^ wait     ^ ticks
```

- After it mounts, the task will wait for `delay` ms before starting to tick
- When the initial delay is passed, the first cycle begins: the task will tick on every frame for `duration` ms. If `duration` is 0, the task will only tick once per cycle.
- Every time the task calls its `tick()` method, its `ticks` property is incremented.
- When the duration is passed, we've reached the end of the current cycle so we increment the `cycles` property and reset `ticks` to 0.
- After that, we wait for `interval` ms before starting the next cycle.

### Properties

- The `loop` property holds a reference to the `Loop` closest to this node in the tree.
```ts
loop: Loop;
```

- The amount of time to wait before starting the first cycle of the task (0 = immediately)
```ts
delay: number;
```

- The amount of time to wait between two successive cycles (0 = immediately).
```ts
interval: number;
```

- The amount of time during which the task will run for every tick (0 = 1 call per cycle).
```ts
duration: number;
```

- The number of cycles of the task that should happen (Infinity = forever).
```ts
repeat: number;
```

- The max number of ticks of the task that should run per cycle.
```ts
limit: number;
```

- The time at which the task was registered.
```ts
startTime: number;
```

- The total number of full cycles executed.
```ts
cycles: number;
```

- The total number of single calls of the tick callback for the current cycle.
```ts
ticks: number;
```

### Lifecycle

A task has a specific set of lifecycle callbacks that are activated when some specific points in the schedule of the task are reached. They are executed as follows:

- The `begin` callback is called when the initial cycle of your task begins. In other words, it means that the `delay` has passed.
```ts
begin(loop: Loop, task: Task): void;
```

- The `beforeCycle` callback is called every time a new cycle of ticks begins, before the first tick of the specified `duration`.
```ts
beforeCycle(loop: Loop, task: Task): void;
```

- The `tick` callback is called for every frame happening during one cycle, for all the time defined in `duration`.
```ts
tick(loop: Loop, task: Task): void;
```

- The `afterCycle` callback is called every time a cycle of ticks ends, after the last tick of the specified `duration`.
```ts
afterCycle(loop: Loop, task: Task): void;
```

- The `end` callback is called when the last cycle of your task ends. In other words, it means that we just passed `duration` after running cycles for the `repeat`-th time.
```ts
end(loop: Loop, task: Task): void;
```

# DOM

The DOM module is useful to define HTML or SVG elements that can be connected inside a `Node` tree.
It exports two functions `html` and `svg` that you can use to quickly create HTML or SVG nodes that render in the DOM.

The `Html` and `Svg` classes are extensions of the `Node` class, and they make sure the tree of `Node` of your app is translated properly into a DOM tree.

When you pass props to these nodes, they are all applied to the DOM element following these rules:
- If the prop name starts with `_`, the prop will be applied as a property of the DOM element.
- If the prop name starts with `$`, the prop will be applied as an event listener
- Otherwise, the prop will be converted to a string and applied as DOM attribute to the element.

Example:
```ts
const div = html("div", { id: "root", _textContent: "Hello", $click: () => alert("click!") })
// will generate a div as:
// <div id="root">Hello</div> + a click event listener that shows an alert
```

```ts
function html<T extends HtmlTag>(tag: T, props?: HtmlProps<Html<T>>, children?: Node[]): Html<T>;
```

```ts
function svg<T extends SvgTag>(tag: T, props?: SvgProps<Svg<T>>, children?: Node[]): Svg<T>;
```

# 2D

The 2d module allows you to create a hierarchy of objects that will render on a canvas.

## Canvas

The `Canvas` node is actually an `Html` node in charge of rendering a `canvas` element in the DOM.
It creates a context in which child `Shape` nodes can be painted.

### Properties

- The 2d context of the canvas
```ts
ctx: CanvasRenderingContext2D;
```

- The width of the canvas
```ts
width: number;
```

- The height of the canvas
```ts
height: number;
```

- You can also use any prop that you would like to see applied on the related `canvas` DOM element, as with every other `Html` node.

### Methods

- Clear the whole canvas content
```ts
clearCanvas(): void
```

## Shape

The `Shape` class allows you to define a node that will paint itself on the closest `Canvas` node in the tree.

### Properties

- The position of the object on the canvas
```ts
position: Vec2
```

### Methods

- This method should be overriden to tell exactly how the shape should be painted on the canvas
```ts
paint(ctx: CanvasRenderingContext2D): void
```

# Input

The input module is a translation of the native javascript events into `Node` classes that integrate well in a loop based program.

## Input

This is the generic input `Node` that provides properties and methods to deal with any kind of input state.
`Input` nodes are made in such a way that if any other `Input` is found in their descendant nodes, the parent `Input` will also react to events happening on descendants. This allows you to build a higher level input class that combines the features of different `Input` subclass, that way you can for example define separate nodes for keyboard, mouse or controller, and then combine them as you see fit.

The `Input` nodes can be used in two different ways: events or live streams.

For example, with the `Mouse` node, when a click is detected, the `Mouse` node will trigger an `input` event that you can subscribe to to react instantly to a click.

On the other hand, imagine you are keeping the up arrow of your keyboard pressed to tell an object to move up on screen. You can't just react to the `keydown` event because it is triggered at intervals that are too long for the movement to be smooth. The `Keyboard` node solves this problem by keeping track of all the keys that are being pressed at any given time. That way you can check them in a `Task` tick, kind of like watching a live stream of the `Keyboard` node. That way, your movement will happen at 60fps as well.

`Input` nodes can define actions: they are supposed to represent the different events and configurations that can be triggered from an input.
The `Keyboard` and `Mouse` nodes have default actions that map directly to the DOM events they watch so you can check them in your loop.

You can also define custom actions on your inputs by filling the `actions` property of your extend `Input` class. The `actions` property is an object that maps action names to a function that checks wether the input currently has a configuration that matches this action. Then, when you call the `isAction()` method of your custom input and give it one of your custom action names as argument, `isAction()` will run the function you associated with this action name to do the check.

You probably should define action names as static properties of your extended `Input` class for easy access and consistency.

### Methods

- Sends an input event
```ts
dispatchInput(action: string, button?: string): void
```

- Subscribe a callback to the input event
```ts
subscribeInput(callback: InputCallback<this>): void
```

- Sends an input event and also update the input current action
```ts
dispatchAction(action: string, button?: string): void
```

- Checks if the params match the current input state, and also checks recursively on descedant `Input` nodes
```ts
isAction(action: string, buttons?: string[]): boolean
```

- Checks if any button of the input is currently pressed
```ts
isActive(): boolean
```

- Checks if the input has all of the given buttons pressed
```ts
hasButtons(buttons?: string[]): boolean
```

## Mouse

This is the `Input` class extended to deal with mouse inputs.

### Actions

- `Mouse.DOWN`: When a mouse button is being pressed down (event)
- `Mouse.HOLD`: When some mouse buttons are being held down (live stream)
- `Mouse.UP`: When a mouse button is released (event)
- `Mouse.WHEEL`: When the mouse wheel is used (event)

### Buttons

- `Mouse.Left`: The mouse left button
- `Mouse.Middle`: The mouse middle button
- `Mouse.Left`: The mouse right button

### Properties

- The x position of the mouse
```ts
x: number;
```

- The y position of the mouse
```ts
y: number;
```

- The distance between the current x and the previous
```ts
dx: number;
```

- The distance between the current y and the previous
```ts
dy: number;
```

- The amount of wheel rolled on the x axis
```ts
wheelX: number;
```

- The amount of wheel rolled on the y axis
```ts
wheelY: number;
```

## Keyboard

`Keyboard` has no specific properties of methods, it simply extends `Input` to deal with keyboard events.

### Actions

- `Keyboard.DOWN`: The first time a key is being pressed down (event)
- `Keyboard.HOLD`: When some keys are being held down (live stream)
- `Keyboard.UP`: When a key is released (event)