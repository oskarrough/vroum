import { Node } from "vroum/core";
import { Input as InputGroup, Keyboard, Mouse } from "vroum/input";

export default class Input extends InputGroup {
  static MOVE_UP = "MOVE_UP";
  static MOVE_DOWN = "MOVE_DOWN";
  static MOVE_LEFT = "MOVE_LEFT";
  static MOVE_RIGHT = "MOVE_RIGHT";
  static MOVE_UP_LEFT = "MOVE_UP_LEFT";
  static MOVE_UP_RIGHT = "MOVE_UP_RIGHT";
  static MOVE_DOWN_LEFT = "MOVE_DOWN_LEFT";
  static MOVE_DOWN_RIGHT = "MOVE_DOWN_RIGHT";
  static SET_TARGET = "SET_TARGET";

  mouse!: Mouse;
  keyboard!: Keyboard;

  actions = {
    [Input.MOVE_UP]: () => this.isAction(Keyboard.HOLD, ["ArrowUp"]),
    [Input.MOVE_DOWN]: () => this.isAction(Keyboard.HOLD, ["ArrowDown"]),
    [Input.MOVE_LEFT]: () => this.isAction(Keyboard.HOLD, ["ArrowLeft"]),
    [Input.MOVE_RIGHT]: () => this.isAction(Keyboard.HOLD, ["ArrowRight"]),
    [Input.MOVE_UP_LEFT]: () => this.isAction(Input.MOVE_UP) && this.isAction(Input.MOVE_LEFT),
    [Input.MOVE_UP_RIGHT]: () => this.isAction(Input.MOVE_UP) && this.isAction(Input.MOVE_RIGHT),
    [Input.MOVE_DOWN_LEFT]: () => this.isAction(Input.MOVE_DOWN) && this.isAction(Input.MOVE_LEFT),
    [Input.MOVE_DOWN_RIGHT]: () => this.isAction(Input.MOVE_DOWN) && this.isAction(Input.MOVE_RIGHT), // prettier-ignore
    [Input.SET_TARGET]: () => this.isAction(Mouse.DOWN, [Mouse.LEFT]),
  };

  mount() {
    this.mouse = this.find(Mouse)!;
    this.keyboard = this.find(Keyboard)!;
  }

  build(children: Node[]) {
    return [
      new Keyboard({}, [
        new Mouse({}, [
          ...children, // nest children at the bottom
        ]),
      ]),
    ];
  }
}
