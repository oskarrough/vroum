import { Task } from "vroum/core";
import { BBox, Vec2 } from "vroum/2d";
import Rect from "./rect";

interface ObstacleProps {
  x: number;
  y: number;
  width: number;
  height: number;
}

export default class Obstacle extends Task<ObstacleProps> {
  rect = Rect.ref();
  bbox = BBox.ref();

  position!: Vec2;
  dimensions!: Vec2;

  init() {
    const x = this.props.x ?? 0;
    const y = this.props.y ?? 0;
    const width = this.props.width ?? 0;
    const height = this.props.height ?? 0;

    this.position = Vec2.of(x, y);
    this.dimensions = Vec2.of(width, height);
  }

  tick() {
    if (!this.bbox.ref.colliding) {
      this.rect.ref.fill = "black";
    }
  }

  build() {
    const { position, dimensions } = this;

    return [
      new BBox({ ref: this.bbox, static: true, position, dimensions }),
      new Rect({ ref: this.rect, fill: "black", position, dimensions }),
    ];
  }
}
