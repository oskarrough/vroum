import "./index.css";
import Game from "./game";

// create the game object and start the loop
const game = Game.start();

// @ts-ignore
window.game = game;
