import { Loop, Task } from "vroum/core";
import { Html, HtmlRef, html } from "vroum/dom";

export default class Timer extends Task {
  // wait 50ms between successive executions of the tasks
  interval = 50;

  timer = Html.ref() as HtmlRef<"span">;
  details = Html.ref() as HtmlRef<"span">;

  play = Html.ref() as HtmlRef<"button">;
  pause = Html.ref() as HtmlRef<"button">;
  stop = Html.ref() as HtmlRef<"button">;

  mount() {
    this.loop.subscribe("play", this.disableButtons);
    this.loop.subscribe("pause", this.disableButtons);
  }

  tick(loop: Loop) {
    const timer = this.timer.ref.element;
    const elapsedTime = (loop.elapsedTime / 1000).toFixed(2);
    timer.textContent = `${elapsedTime}s `;

    const details = this.details.ref.element;
    const fps = loop.deltaTime > 0 ? Math.round(1000 / loop.deltaTime) : 0;
    details.textContent = `${fps} fps`;
  }

  destroy() {
    this.loop.unsubscribe("play", this.disableButtons);
    this.loop.unsubscribe("pause", this.disableButtons);
  }

  disableButtons = () => {
    this.play.ref.set({ disabled: !this.loop.paused });
    this.pause.ref.set({ disabled: this.loop.paused });
  };

  build() {
    return [
      html("section", { class: "menu" }, [
        html("span", { ref: this.timer, class: "time", _textContent: "0ms" }),
        html("span", { ref: this.details, _textContent: "0 fps" }),
        html("div", { class: "buttons" }, [
          html("button", {
            ref: this.play,
            disabled: true,
            _textContent: "Play",
            $click: () => this.loop.play(),
          }),
          html("button", {
            ref: this.pause,
            _textContent: "Pause",
            $click: () => this.loop.pause(),
          }),

          html("button", {
            ref: this.stop,
            _textContent: "Stop",
            $click: () => this.loop.stop(),
          }),
        ]),
      ]),
    ];
  }
}
