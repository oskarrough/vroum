import { Loop } from "vroum/core";
import { Canvas } from "vroum/2d";
import Timer from "./timer";
import Player from "./player";
import Obstacle from "./obstacle";

export default class Game extends Loop {
  canvas = Canvas.ref();

  tick() {
    this.canvas.ref.clearCanvas();
  }

  build() {
    return [
      new Timer(),
      new Canvas({ ref: this.canvas, width: 640, height: 640, class: "game" }, [
        new Obstacle({ x: 320, y: 0, width: 640, height: 1 }), // top border
        new Obstacle({ x: 639, y: 320, width: 1, height: 640 }), // right border
        new Obstacle({ x: 320, y: 639, width: 640, height: 1 }), // bottom border
        new Obstacle({ x: 0, y: 320, width: 1, height: 640 }), // left border
        new Obstacle({ x: 100, y: 200, width: 100, height: 300 }),
        new Obstacle({ x: 100, y: 540, width: 150, height: 150 }),
        new Obstacle({ x: 450, y: 540, width: 200, height: 60 }),
        new Obstacle({ x: 500, y: 150, width: 160, height: 140 }),
        new Player({ x: 320, y: 320 }),
      ]),
    ];
  }
}
