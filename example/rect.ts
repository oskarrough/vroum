import { Shape, Vec2 } from "vroum/2d";

interface RectProps {
  dimensions?: Vec2;
  fill?: string;
  stroke?: string;
}

export default class Rect extends Shape<RectProps> {
  origin!: Vec2;
  dimensions!: Vec2;
  fill?: string;
  stroke?: string;

  init() {
    this.dimensions = this.props.dimensions ?? Vec2.of(0);
    this.fill = this.props.fill;
    this.stroke = this.props.stroke;
  }

  paint(ctx: CanvasRenderingContext2D) {
    const { x, y } = Vec2.origin(this.position, this.dimensions).apply(Math.round);
    const { x: width, y: height } = this.dimensions;

    if (this.fill) {
      ctx.fillStyle = this.fill;
      ctx.fillRect(x, y, width, height);
    }

    if (this.stroke) {
      ctx.strokeStyle = this.stroke;
      ctx.strokeRect(x, y, width, height);
    }
  }
}
