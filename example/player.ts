import { Loop, Task } from "vroum/core";
import { BBox, Vec2 } from "vroum/2d";
import Input from "./input";
import Rect from "./rect";
import Obstacle from "./obstacle";

interface PlayerProps {
  x?: number;
  y?: number;
}

export default class Player extends Task<PlayerProps> {
  static SPEED = 0.3;
  static SIZE = 32;

  position!: Vec2;
  dimensions!: Vec2;

  target!: Vec2;

  input = Input.ref();
  bbox = BBox.ref();
  rect = Rect.ref();
  mark = Rect.ref();

  init() {
    const x = this.props.x ?? 0;
    const y = this.props.y ?? 0;

    this.position = Vec2.of(x, y);
    this.dimensions = Vec2.of(Player.SIZE);
    this.target = Vec2.of(-Infinity);
  }

  mount() {
    this.bbox.ref.subscribeCollision(this.onCollision);
    this.input.ref.subscribeInput(this.onInput);
  }

  tick(loop: Loop) {
    this.moveWithArrows(loop.deltaTime);
    this.moveTowardsTarget(loop.deltaTime);

    if (!this.bbox.ref.colliding) {
      this.rect.ref.fill = "limegreen";
    }
  }

  destroy() {
    this.bbox.ref.unsubscribeCollision(this.onCollision);
    this.input.ref.unsubscribeInput(this.onInput);
  }

  moveWithArrows(time: number) {
    const input = this.input.ref;

    const orthogonal = Player.SPEED * time;
    const diagonal = orthogonal / Math.SQRT2;

    if (input.isAction(Input.MOVE_UP_LEFT)) {
      this.position.x -= diagonal;
      this.position.y -= diagonal;
    } //
    else if (input.isAction(Input.MOVE_UP_RIGHT)) {
      this.position.x += diagonal;
      this.position.y -= diagonal;
    } //
    else if (input.isAction(Input.MOVE_DOWN_LEFT)) {
      this.position.x -= diagonal;
      this.position.y += diagonal;
    } //
    else if (input.isAction(Input.MOVE_DOWN_RIGHT)) {
      this.position.x += diagonal;
      this.position.y += diagonal;
    } //
    else if (input.isAction(Input.MOVE_UP)) {
      this.position.x += 0;
      this.position.y -= orthogonal;
    } //
    else if (input.isAction(Input.MOVE_DOWN)) {
      this.position.x += 0;
      this.position.y += orthogonal;
    } //
    else if (input.isAction(Input.MOVE_LEFT)) {
      this.position.x -= orthogonal;
      this.position.y += 0;
    } //
    else if (input.isAction(Input.MOVE_RIGHT)) {
      this.position.x += orthogonal;
      this.position.y += 0;
    }
  }

  moveTowardsTarget(time: number) {
    if (this.target.is(-Infinity)) return;

    const distance = Vec2.from(this.target).sub(this.position);

    // target is reached
    if (distance.norm() < Player.SIZE / 4) {
      this.position.copy(this.target);
      this.target.set(-Infinity);
      return;
    }

    const step = distance.unit().scale(Player.SPEED * time);
    this.position.add(step);
  }

  onInput = (input: Input) => {
    if (input.isAction(Input.SET_TARGET)) {
      this.target.copy(input.mouse.position);
    }
  };

  onCollision = (self: BBox, other: BBox) => {
    const player = self.closest(Player); // just for the example, `this` should be used instead
    const obstacle = other.closest(Obstacle);

    if (!obstacle || !player) return;

    const playerRect = player.rect.ref;
    const obstacleRect = obstacle.rect.ref;

    playerRect.fill = "red";
    obstacleRect.fill = "orange";
  };

  build() {
    const { position, dimensions } = this;

    return [
      new Input({ ref: this.input }),
      new BBox({ ref: this.bbox, position, dimensions }),
      new Rect({ ref: this.rect, position, dimensions, fill: "limegreen" }),
      new Rect({ ref: this.mark, position: this.target, dimensions, stroke: "limegreen" }),
      new PulseAnimation(),
    ];
  }
}

class PulseAnimation extends Task {
  declare parent: Player;

  repeat = 2 * 3; // repeat the grow + shrink animations 3 times

  delay = 1000;
  duration = 1000;
  interval = 500;

  tick(loop: Loop) {
    const step = loop.deltaTime / 16;
    const direction = this.cycles % 2 === 0 ? 1 : -1;

    this.parent.dimensions.translate(step * direction).apply(Math.round);
  }
}
