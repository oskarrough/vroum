import { resolve } from "path";
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";

export default defineConfig({
  plugins: [dts({ insertTypesEntry: true, exclude: ["./example"] })],

  resolve: {
    alias: {
      vroum: resolve(__dirname, "lib"),
    },
  },

  build: {
    lib: {
      name: "vroum",
      formats: ["es"],
      fileName: (_, entry) => `${entry}.js`,

      entry: {
        index: resolve(__dirname, "lib/index.ts"),
        core: resolve(__dirname, "lib/core/index.ts"),
        dom: resolve(__dirname, "lib/dom/index.ts"),
        "2d": resolve(__dirname, "lib/2d/index.ts"),
        input: resolve(__dirname, "lib/input/index.ts"),
      },
    },
  },
});
